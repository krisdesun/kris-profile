from django import forms
from .models import jadwal

class FormJadwal(forms.Form):
	nama = forms.CharField(max_length=100,
							widget=forms.TextInput(attrs={
								'class':'form-control',
								'placeholder':'Masukkan nama mata kulliah'
								}))
	dosen = forms.CharField(max_length=100,
							widget=forms.TextInput(attrs={
								'class':'form-control',
								'placeholder':'Masukkan nama dosen'
								}))
	sks = forms.CharField(max_length=10,
							widget=forms.TextInput(attrs={
								'class':'form-control',
								'placeholder':'Masukkan jumlah sks'
								}))
	ruang = forms.CharField(max_length=10,
							widget=forms.TextInput(attrs={
								'class':'form-control',
								'placeholder':'Masukkan nomor ruangan'
								}))
	desc = forms.CharField(widget=forms.Textarea(attrs={
								'class':'form-control',
								'placeholder':'Deskripsi'
								}))